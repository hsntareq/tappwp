<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'codergen_wp135' );

/** MySQL database username */
define( 'DB_USER', 'codergen_wp135' );

/** MySQL database password */
define( 'DB_PASSWORD', ']T1.pSI460' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'fjwewhyhcunoi32bjttcu1gqb4mzi6gw6p7prasfokqqecoyzkpe5fhmiad7pemp' );
define( 'SECURE_AUTH_KEY',  '4kjgupylxxol4gcqmal0fzin5hawyycnmitqwjhzli4n39xmwabcaenekmsbhock' );
define( 'LOGGED_IN_KEY',    'xilhey8cbv1hbsat3ejn4jqrpmtkd4pzqrm7k4icmuzyua2ai7vaf6zpktq4yeaa' );
define( 'NONCE_KEY',        '0pu5tlxr1yodkz6zqi4fblou0r6x6tcj9xgdbvqzkuwjijil7kmysadg1zvyv5qo' );
define( 'AUTH_SALT',        '53wzfsamgrstszssrgm213nzclgizuf8eyxoiblk5acvwy2eeovkpdrxomuxiujn' );
define( 'SECURE_AUTH_SALT', 'c8wmdnfnllb5rwazffzckgxyquru1rfd7k8zivktm1rs9dzswckzbcee0cxsmx5y' );
define( 'LOGGED_IN_SALT',   'qujtsjhrf6bbwos1btnlcbwmahyi2yzudysdmlyyhggcoetyewruqvg4cjvvhlwc' );
define( 'NONCE_SALT',       'prm6xkss0gxjjnijjt12r1ihevtfoxmcni0txn9fgwiwterdtgpcierojbtwvxys' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp2s_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
